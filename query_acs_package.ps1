<#
Simple powershell script to query ACS for a specific tracking number

Usage (Powershell):

`query_acs_package.ps1 [-n] <tracking number>`

Currently only returns True if the package is found or not
#>

#Get the tracking number as a named argument
param(
    [string]$n = ""
)

if($n -eq "")
{
    Write-Host "Empty tracking number supplied!" -ForegroundColor red
    exit 1
}

#Set TLS to version 1.2
[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12

# Make the actual request
Write-Host "Quering the ACS server for tracking number $n.."
$r = Invoke-RestMethod -Method Post -Uri "https://www.acscourier.net/en/track-and-trace?p_p_id=ACSCustomersAreaTrackTrace_WAR_ACSCustomersAreaportlet&p_p_lifecycle=1&p_p_state=maximized&p_p_mode=view&_ACSCustomersAreaTrackTrace_WAR_ACSCustomersAreaportlet_javax.portlet.action=trackTrace" -Body "jspPage=TrackTraceController&generalCode=$n"

if($r -match "No results for your query")
{
    Write-Host "Package not found!" -ForegroundColor yellow
    exit 2
}
else
{
    Write-Host "Package found! Must be on its way." -ForegroundColor green
    exit 0
}
